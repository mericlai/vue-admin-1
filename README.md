# 项目说明 #

``` bash
  本项目作为公司管理系统/平台项目模板，根据公司系统界面风格所做的一套统一模板。项目使用Vue + vue-router + axios + jquery(由于使用ztree和layer) + layer(弹窗提示库) + ztree(在地址本用到)，以及自己编写的组件：地址本、表单验证、树形组件、日历组件、分页组件。
```
----------
## 项目结构 ##
```bash
│  .babelrc
│  .editorconfig
│  .gitignore
│  .postcssrc.js
│  COMPONENTS-README.md
│  favicon.ico
│  index.html
│  package.json
│  README.md
│  yarn.lock
│
├─build     //存放webpack配置文件
│      build.js
│      check-versions.js
│      logo.png
│      utils.js
│      vue-loader.conf.js
│      webpack.base.conf.js
│      webpack.dev.conf.js
│      webpack.prod.conf.js
│
├─config    //环境编译配置文件
│      dev.env.js
│      index.js
│      prod.env.js
│
├─src   //项目源码根目录
│  │  App.vue
│  │  main.js
│  │
│  ├─api    //接口访问源
│  ├─assets     //静态资源文件(会被webpack编译，引用时使用相对路径)
│  │  │  .DS_Store
│  │  │  logo.png
│  │  │
│  │  ├─css     //样式文件
│  │  │      bootstrap-theme.css
│  │  │      bootstrap-theme.css.map
│  │  │      bootstrap-theme.min.css
│  │  │      bootstrap-theme.min.css.map
│  │  │      bootstrap.css
│  │  │      bootstrap.css.map
│  │  │      bootstrap.min.css
│  │  │      bootstrap.min.css.map
│  │  │      common.scss
│  │  │
│  │  ├─fonts       //字体文件
│  │  │      glyphicons-halflings-regular.eot
│  │  │      glyphicons-halflings-regular.svg
│  │  │      glyphicons-halflings-regular.ttf
│  │  │      glyphicons-halflings-regular.woff
│  │  │      glyphicons-halflings-regular.woff2
│  │  │
│  │  └─images      //图片文件
│  │          logo.png
│  │
│  ├─components     //公共组件
│  │  │  AddressBook.vue
│  │  │  DropDownFilter.vue
│  │  │  HelloWorld.vue
│  │  │  LeftMenu.vue
│  │  │  loading.vue
│  │  │  Pagination.vue
│  │  │
│  │  ├─tree    //树形组件
│  │  │      node.js
│  │  │      store.js
│  │  │      table-tree-node.vue
│  │  │      table-tree.vue
│  │  │      tree-node.vue
│  │  │      tree.vue
│  │  │      util.js
│  │  │
│  │  └─vue2-slot-calendar  //日历组件
│  │          Calendar.vue
│  │
│  ├─permission     //权限/拦截器
│  │      index.js
│  │
│  ├─router     //路由配置
│  │      index.js
│  │
│  ├─utils      //工具类
│  │      Cookie.js
│  │      emitter.js
│  │      fetch.js
│  │      index.js
│  │      validator-directive.js
│  │      validators.js
│  │
│  └─views      //vue页面
│          Container.vue
│          Demo.vue
│          List.vue
│
└─static    //静态资源文件(不会被webpack编译，引用时必须使用绝对路径。建议只放第三方插件)
    │  .gitkeep
    │  layer.js     //layer弹窗插件
    │
    ├─theme //layer弹窗静态资源文件
    │  │  .DS_Store
    │  │
    │  └─default
    │          icon-ext.png
    │          icon.png
    │          layer.css
    │          loading-0.gif
    │          loading-1.gif
    │          loading-2.gif
    │
    └─zTree_v3      //ztree插件
        ├─css
        │  └─metroStyle
        │      │  metroStyle.css
        │      │
        │      └─img
        │              line_conn.png
        │              loading.gif
        │              metro.gif
        │              metro.png
        │
        └─js
                jquery-1.4.4.min.js
                jquery.ztree.all.js
                jquery.ztree.all.min.js
                jquery.ztree.core.js
                jquery.ztree.core.min.js
                jquery.ztree.excheck.js
                jquery.ztree.excheck.min.js
                jquery.ztree.exedit.js
                jquery.ztree.exedit.min.js
                jquery.ztree.exhide.js
                jquery.ztree.exhide.min.js
```
> ### Tip
>
```bash
1.assets和static区别

在 *.vue 组件中，所有模板和CSS都会被 vue-html-loader 及 css-loader 解析，并查找资源URL。例如，在 <img src="./logo.png">
和 background: url(./logo.png) 中，"./logo.png" 是相对的资源路径，将由Webpack解析为模块依赖。  
因为 logo.png 不是 JavaScript，当被视为模块依赖时，需要使用 url-loader 和 file-loader
处理它。vue-cli 的 webpack 脚手架已经配置了这些 loader，因此可以使用相对/模块路径。  
由于这些资源可能在构建过程中被内联/复制/重命名，所以它们基本上是源代码的一部分。这就是为什么建议将
Webpack 处理的静态资源放在 /src 目录中和其它源文件放一起的原因。事实上，甚至不必把它们全部放在 /src/assets：可以用模块/组件的组织方式来使用它们。例如，可以在每个放置组件的目录中存放静态资源。  
"Real" Static Assets
相比之下，static/ 目录下的文件并不会被 Webpack 处理：它们会直接被复制到最终目录（默认是dist/static）下。必须使用绝对路径引用这些文件，这是通过在 config.js 文件中的 build.assetsPublicPath 和 build.assetsSubDirectory 连接来确定的。  
任何放在 static/ 中文件需要以绝对路径的形式引用：/static/[filename]。如果更改 assetSubDirectory 的值为 assets，那么路径需改为 /assets/[filename]。  
详请请查看webpack对此的说明：https://athena0304.gitbooks.io/vue-template-webpack-cn/content/static.html
```
>

----------
## 日历控件 ##

![](/src/components/vue2-slot-calendar/demo.gif)

#### 使用说明 ####

```html
    <calendar
    :value="value"
    :disabled-days-of-week="disabled"
    :format="format"
    :clear-button="clear"
    :placeholder="placeholder"
    :pane="2"
    :has-input="false"
    :on-day-click="onDayClick2"
    :special-days="_dateMap"
  ></calendar>

```
#### 异步数据使用说明 ####

```html
<calendar class="event-calendar" :value="value" :disabled-days-of-week="disabled" :format="format" :clear-button="clear" :placeholder="placeholder" :pane="2" :has-input="false" :on-day-click="onDayClick3" :change-pane="changePane">
    <div v-for="evt in events" :slot="evt.date">
        ${{evt.content}} <i :class="{low : evt.low}" v-if="evt.low">↓</i>
    </div>
  </calendar>
```
>具体说明：https://github.com/icai/vue2-calendar

> 例子：http://blog.w3cub.com/vue2-calendar/
----------
## layer弹框 ##
> 使用说明详见：http://layer.layui.com/api.html
----------
## 人员选择框 ##
> 人员选择框使用layer弹框结合ztree组成,依赖jQuery
> 使用说明查看Helloworld.vue文件demo的注释。
```javascript
showAddressBookDialog(){
    let layerDialog = Utils.layerBox.layerDialogOpen({
        title: '人员选择器',
        btn:['确定','取消'],
        maxmin: false, //开启最大化最小化按钮
        area: ['860px', '590px'],
        btn1:(index,layero)=>{
        this.dialogComponent&&this.dialogComponent.$children[0].doSubmit&&this.dialogComponent.$children[0].doSubmit();
        },
        btn2:(index,layero)=>{
        this.dialogComponent&&this.dialogComponent.$children[0].doCancel&&this.dialogComponent.$children[0].doCancel();
        }
    });
    this.dialogComponent = Utils.layerBox.layerLoadVueComponent({layer:layerDialog,component:AddressBook,methods:{
        //必要参数
        doComfirm:(result)=>{
            //doComfirm可获取组件页面传过来的参数，作用：例如执行回调方法或者传递结果到父组件
            this.selectList = result
            layer.close(layerDialog);//关闭对话框
        },
        //必要参数
        doClose:()=>{
            layer.close(layerDialog);//关闭对话框
        }
        },
        //传递本组件的参数给对话框组件，对话框组件通过props属性params获取值,例如下面这个val属性取值：this.params.val
        props:{
        type:'2',//1:部门,2://人员,3:部门和人员
        }});
},
```
------------
## 表单验证
> 在src/main.js入口文件引入
```javascript
import Validator from '@/utils/validator-directive'

new Validator(Vue,{timeout:2000}).install(Vue);//加载安装表单验证指令
```
> 验证器由`validator-directive.js`和`validators.js`组成。  
`validator-directive.js`主要为指令安装方法和验证触发事件绑定；`validators.js`则是表单常用验证规则，包括类型为`number,required,email,date,length,validator,url`。其中validator类型为自定义验证方式。   
下面是使用方式：
```html
<template>
  <div class="app-management-edit-wrap">
    <form class="form-horizontal" style="padding:20px;width:600px;">
        <div class="form-group">
            <label class="col-xs-3 text-right">应用名称：</label>
            <div class="col-xs-7">
                <input v-validate="rules.fdName" v-model="form.fdName" class="form-control" placeholder="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-xs-3 text-right">应用域名：</label>
            <div class="col-xs-7">
                <input class="form-control" v-validate="rules.fdDomain" v-model="form.fdDomain" placeholder="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-xs-3 text-right">端口号：</label>
            <div class="col-xs-7">
                <input class="form-control" v-validate="rules.fdPort" v-model="form.fdPort" placeholder="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-xs-3 text-right">应用路径：</label>
            <div class="col-xs-7">
                <input class="form-control"  v-validate="rules.fdPath" v-model="form.fdPath" placeholder="" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-3"></div>
            <div class="col-xs-7 text-left">
                <button class="btn btn-primary" type="button" @click="doSubmit">提交</button>
                <button class="btn btn-default" type="button" @click="goBack">返回</button>
            </div>
        </div>
    </form>
  </div>
</template>

<script>
export default {
  data() {
    const formName = "appForm";
    return {
      dialogComponent: null,
      form: {
        fdName: "",
        fdDomain: "",
        fdPort: "",
        fdPath: "",
      },
      rules: {
        fdName: {
          rule: [
            {
              trigger: "blur",
              type: "validator",
              validate: value => {
                if (!value) return { success: false, msg: "应用名称不能为空" };
                return new Promise(resolve => {
                  checkName({
                    fdName: value,
                    fdId: this.$route.params.id || ""
                  })
                    .then(res => {
                      resolve(true);
                    })
                    .catch(err => {
                      resolve({ success: err.success, msg: err.msg });
                    });
                });
              }
            }
          ],
          form: formName
        },
        fdDomain: {
          rule: [
              { trigger: "blur", type: "required", message: "请输入应用域名" },
              { trigger: "change", type: "url", message: "请输入正确应用域名" }
            ],
          form: formName
        },
        fdPort: {
          rule: [
            {
              trigger: "change",
              type: "number",
              message: "请输入正确的端口"
            },
            {
              trigger: "blur",
              type: "validator",
              validate: function(value) {
                return {
                  success:
                    String(value).length < 2 ? false : true,
                  msg: "请输入最少两个字符"
                };
              }
            }
          ],
          form: formName
        },
        fdPath: {
          rule: [
            {
              trigger: "blur",
              type: "validator",
              validate: value => {
                if (!value) return { success: false, msg: "应用路径不能为空" };
                return new Promise(resolve => {
                  checkPath({
                    fdPath: value,
                    fdId: this.$route.params.id || ""
                  })
                    .then(res => {
                      resolve(true);
                    })
                    .catch(err => {
                      resolve({ success: err.success, msg: err.msg });
                    });
                });
              }
            }
          ],
          form: formName
        }
      }
    };
  },
  methods: {
    doSubmit() {
      this.$validator.submit("appForm").then(valid => {
        if (valid) {
            //do submit action
          } else {
            //do something
          }
        }
      });
    }
  }
};
</script>
```
> 验证指令为 v-validate。使用方式：`<input v-validate="rules.fdName" v-model="form.fdName" class="form-control" placeholder="" />`，`v-validate="rules.fdName"`的`rules.fdName`是验证规则，在data方法里面设置:
```javascript
rules: {
    fdName: {
        rule: [
        {
            trigger: "blur",
            type: "validator",
            validate: value => {
            if (!value) return { success: false, msg: "应用名称不能为空" };
            return new Promise(resolve => {
                checkName({
                fdName: value,
                fdId: this.$route.params.id || ""
                })
                .then(res => {
                    resolve(true);
                })
                .catch(err => {
                    resolve({ success: err.success, msg: err.msg });
                });
            });
            }
        }
        ],
        form: formName
    },
}
```
>fdName里面有rule,form两个属性。
>>rule表示验证规则。rule对象里面的属性有
* `trigger`:触发方式，同js事件类型，如`click`,`change`,`blur`等。
* `type`:验证类型，现有常有类型`number,required,email,date,length,validator,url`,其中只有`validator`类型需与属性`validate`结合使用,支持异步验证，异步验证例子请看上方fdName验证规则，非异步验证例子请看fdPort验证规则,
* `validate`:自定义验证规则，错误信息返回`Object`或者`Boolean`对象，如`{ success: err.success, msg: err.msg }`(`success`为`Boolean`类型，`msg`为错误提示字符串)或 `false`，验证通过则只需返回`true`,注意异步验证需结合`Promise`使用，返回需为`resolve({ success: err.success, msg: err.msg })`或者`resolve(false|true)`。  
>>form表示所要验证的表单名称，验证时将验证表单名称相同的表单控件。

### 提交验证
 ```javascript
doSubmit() {
      //appForm为rules验证规则属性form的值，表示验证哪个表单，valid为返回结果true或者false
      this.$validator.submit("appForm").then(valid => {
        if (valid) {
            //do submit action
          } else {
            //do something
          }
        }
      });
    }
```
-------
## 树形组件

-------
For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```



