'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_API:'"http://console.hwagain.com:60003/audit"',
  STRUCTURE_BASE_URL:'"http://console-sit.hwagain.com:60000/hwagain"',
  LOGIN_PATH:'"http://console-sit.hwagain.com:10000/#/login?url="',
  // MENU_PATH:'"http://framework.hwagain.com/hwagain/enter/index.html?systemCode=audit"'
})
