let nodeId = 0;

export const getChildState = node => {
  let all = true; //是否选上所有选择框
  let none = true; //是否所有选择框都没选上
  let allWithoutDisable = true; //是否所有选择框都是不可点击
  for (let i = 0, j = node.length; i < j; i++) {
    const n = node[i];
    if (n.checked !== true || n.indeterminate) {
      all = false;
      if (!n.disabled) {
        allWithoutDisable = false;
      }
    }
    if (n.checked !== false || n.indeterminate) {
      none = false;
    }
  }
  return { all, none, allWithoutDisable, half: !all && !none };
};

const reInitChecked = function(node) {
  if (node.childNodes.length === 0) return;

  const { all, none, half } = getChildState(node.childNodes);
  if (all) {
    node.checked = true;
    node.indeterminate = false;
  } else if (half) {
    node.checked = false;
    node.indeterminate = true;
  } else if (none) {
    node.checked = false;
    node.indeterminate = false;
  }

  const parent = node.parent;
  if (!parent || parent.level === 0) return;
  if (!node.store.checkStrictly) {
    reInitChecked(parent);
  }
};

const objectAssign = function(target) {
  for (let i = 1, j = arguments.length; i < j; i++) {
    let source = arguments[i] || {};
    for (let prop in source) {
      if (source.hasOwnProperty(prop)) {
        let value = source[prop];
        if (value !== undefined) {
          target[prop] = value;
        }
      }
    }
  }

  return target;
};

const getPropertyFromData = function(node, prop) {
  const props = node.store.props;
  const data = node.data || {};
  const config = props[prop];
  if (typeof config === "function") {
    return config(data, node);
  } else if (typeof config === "string") {
    return data[config];
  } else if (typeof config === "undefined") {
    const dataProp = data[prop];
    return dataProp === undefined ? "" : dataProp;
  }
};

import { markNodeData } from "./util";
export default class Node {
  constructor(options) {
    this.id = nodeId++;
    this.checked = false;
    this.data = null;
    this.expanded = false;
    this.parent = null;

    for (let option in options) {
      this[option] = options[option];
    }
    this.level = 0;
    this.loaded = false;
    this.childNodes = [];
    this.loading = false;

    if (this.parent) {
      this.level = this.parent.level + 1;
    }

    const store = this.store;
    if (!store) {
      throw new Error("[Node]store is required!");
    }
    store.registerNode(this); //存储node到nodesMap
    const props = store.props;
    if (props && typeof props.isLeaf !== "undefined") {
      const isLeaf = this["isLeaf"];
      if (typeof isLeaf === "boolean") {
        this.isLeafByUser = isLeaf;
      }
    }
    if (store.lazy !== true && this.data) {
      this.setData(this.data);
      if (store.defaultExpandAll) {
        this.expanded = true;
      }
    } else if (this.level > 0 && store.lazy && store.defaultExpandAll) {
      this.expand();
    }
    if (!this.data) return;
    const defaultExpandedKeys = store.defaultExpandedKeys;
    const key = store.key;
    if (
      key &&
      defaultExpandedKeys &&
      defaultExpandedKeys.indexOf(this.key) != -1
    ) {
      this.expand(null, store.autoExpandParent);
    }
    if (
      key &&
      store.currentNodeKey !== undefined &&
      this.key === store.currentNodeKey
    ) {
      store.cureentNode = this;
    }
    if (store.lazy) {
      store._initDefaultCheckedNode(this);
    }
    this.updateLeafState();
  }
  setData(data) {
    if (!Array.isArray(data)) {
      markNodeData(this, data);
    }
    this.data = data;
    this.childNodes = [];
    let children;
    if (this.level === 0 && this.data instanceof Array) {
      children = this.data;
    } else {
      children = getPropertyFromData(this, "children") || [];
    }
    for (let i = 0, j = children.length; i < j; i++) {
      this.insertChild({ data: children[i] });
    }
  }
  get key() {
    const nodeKey = this.store.key;
    if (this.data) return this.data[nodeKey];
    return null;
  }
  get label() {
    return getPropertyFromData(this,'label');
  }
  get icon() {
    return getPropertyFromData(this,'icon');
  }
  get disabled() {
    return getPropertyFromData(this, "disabled") == true;
  }
  createNodeChildren(arr, defaultProps = {}) {
    arr.forEach(item => {
      this.insertChild(
        objectAssign({ data: item }, defaultProps),
        undefined,
        true
      );
    });
  }
  removeChild(child) {
    const children = this.getChildren() || [];
    const dataIndex = children.indexOf(child.data);
    if (dataIndex > -1) {
      children.splice(dataIndex, 1);
    }
    const index = this.childNodes.indexOf(child);
    if (index > -1) {
      this.store && this.store.deregisterNode(child);
      child.parent = null;
      this.childNodes.splice(index, 1);
    }
    this.updateLeafState();
  }
  insertChild(child, index, batch) {
    if (!child) throw new Error("insertChild error:child is required.");
    if (!(child instanceof Node)) {
      if (!batch) {
        const children = this.getChildren() || [];
        if (children.indexOf(child.data) === -1) {
          if (typeof index === "undefined" || index < 0) {
            children.push(child.data);
          } else {
            children.splice(index, 0, child.data);
          }
        }
      }
      objectAssign(child, {
        parent: this,
        store: this.store
      });
      child = new Node(child);
    }
    child.level = this.level + 1;
    if (typeof index === "undefined" || index < 0) {
      this.childNodes.push(child);
    } else {
      this.childNodes.splice(index, 0, child);
    }
    this.updateLeafState();
  }
  getChildren() {
    if (this.level === 0) return this.data;
    const data = this.data;
    if (!data) return null;

    const props = this.store.props;
    let childrenKey = "children";
    if (props) {
      childrenKey = props.children || "children";
    }

    if (data[childrenKey] === undefined) {
      data[childrenKey] = null;
    }
    return data[childrenKey];
  }
  updateLeafState() {
    if (this.store.lazy === true && this.loaded === true) {
      this.isLeaf = this.isLeafByUser;
      return;
    }
    const childNodes = this.childNodes;
    if (!this.store.lazy || (this.store.lazy == true && this.loaded === true)) {
      this.isLeaf = !childNodes || childNodes.length === 0;
      return;
    }
    this.isLeaf = false;
  }
  setChecked(value, deep, recursionFlag, passValue) {
    this.indeterminate = value === "half";
    this.checked = value === true;
    if (this.store.checkStrictly) return;

    if (!this.shouldLoadData() && this.store.checkDescendants) {
      let { all, allWithoutDisable } = getChildState(this.childNodes);
      if (!this.isLeaf && (!all && allWithoutDisable)) {
        this.checked = false;
        value = false;
      }

      const handleDescendants = () => {
        if (deep) {
          const childNodes = this.childNodes;
          for (let i = 0, j = childNodes.length; i < j; i++) {
            const child = childNodes[i];
            passValue = passValue || value !== false;
            const isCheck = child.disabled ? child.checked : passValue;
            child.setChecked(isCheck, deep, true, passValue);
          }
          const { half, all } = getChildState(childNodes);
          if (!all) {
            this.checked = all;
            this.indeterminate = half;
          }
        }
      };

      if (this.shouldLoadData()) {
        //only work on lazy load date.
        this.loadData(
          () => {
            handleDescendants();
            reInitChecked(this);
          },
          {
            checked: value !== false
          }
        );
        return;
      } else {
        handleDescendants();
      }
    }
    const parent = this.parent;
    if (!parent || parent.level === 0) return;
    if (!recursionFlag) {
      reInitChecked(parent);
    }
  }
  shouldLoadData() {
    return this.store.lazy === true && this.store.load && !this.loaded;
  }
  loadData(cb, defaultProps = {}) {
    if (
      this.store.lazy === true &&
      this.store.load &&
      !this.loaded &&
      (!this.loading || Object.keys(defaultProps).length)
    ) {
      this.loading = true;

      const resolve = children => {
        this.loaded = true;
        this.loading = false;
        this.childNodes = [];

        this.createNodeChildren(children, defaultProps);
        this.updateLeafState();
        if (cb) cb.call(this, children);
      };
      this.store.load(this, resolve);
    } else {
      if (cb) cb.call(this);
    }
  }
  expand(cb, expandParent) {
    const done = () => {
      if (expandParent) {
        let parent = this.parent;
        while (parent.level > 0) {
          parent.expanded = true;
          parent = parent.parent;
        }
      }
      this.expanded = true;
      cb && cb();
    };
    if (this.shouldLoadData()) {
      this.loadData(data => {
        if (data instanceof Array) {
          if (this.checked) {
            this.setChecked(true, true);
          } else {
            reInitChecked(this);
          }
          done();
        }
      });
    } else {
      done();
    }
  }
  collapse() {
    this.expanded = false;
  }
}
