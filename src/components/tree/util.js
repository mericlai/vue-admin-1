const NODE_KEY = '$treeNodeId';
export const markNodeData = function(node, data) {
    if (data[NODE_KEY]) return;
    Object.defineProperty(data, NODE_KEY, {
      value: node.id,
      enumerable: false,
      configurable: false,
      writable: false
    });
};
export const getNodeKey = function(key, data){
    if(!key) return data[NODE_KEY];
    return data[key];
}
export const createUniqueString = function() {
    const timestamp = +new Date() + ''
    const randomNum = parseInt((1 + Math.random()) * 65536) + ''
    return (+(randomNum + timestamp)).toString(32)
}