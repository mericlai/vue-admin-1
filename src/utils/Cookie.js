import Cookies from 'js-cookie'

export const TokenKey = 'hwagain_sso_token'
export const UserKey = 'user';
export function getCookie(CookieKey) {
  return Cookies.get(CookieKey) || ''
}

export function setCookie(CookieKey,CookieValue) {
  return Cookies.set(CookieKey, CookieValue, {domain:'.hwagain.com',path:'/'})
}

export function removeCookie(CookieKey) {
  return Cookies.remove(CookieKey)
}
