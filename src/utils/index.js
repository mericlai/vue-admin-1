import Vue from 'vue'

export const layerBox = {
    layerDialogOpen(options){
        return layer.open({
            type: 1,
            title: options.title||'提示',
            shadeClose: options.shadeClose || false,
            shade: options.shade||true,
            resize:options.resize||false,
            shade:.4,
            btn:options.btn||['确定','取消'],
            maxmin: options.maxmin||false, //开启最大化最小化按钮
            area: options.area||['693px', '500px'],
            btn1:options.btn1 || null,
            btn2:options.btn2 || null,
            content:options.content || ""
        })
    },
    /**
     * 
     * @param {*} options 
     * options = {
     *  layer:layer弹框实例,
     *  component:Vue组件,
     *  props:props，数组类型,
     *  methods:'一些方法，可在component组件里面调用，对象类型'，
     * }
     */
    layerLoadVueComponent(options){
        $('#layui-layer'+options.layer+' .layui-layer-content').append('<div></div>')
        let components = {};
        components['component'+options.layer] = options.component;
        const component = new Vue({
            el:'#layui-layer'+options.layer+' .layui-layer-content>div',
            data:options.props || {},
            methods:options.methods,
            components,
            template:`<component${options.layer} @submit-click='doComfirm' @cancel-click='doClose' :params='$data'></component${options.layer}>`
            // render: h => h(options.component)
        });
        return component;
    },
}