import Vue from 'vue'
import Router from 'vue-router'

const loadComponent = (fileName) => {
  return () => import('@/components/'+fileName);
}
const loadViews = (fileName) =>{
  return () => import('@/views/'+fileName);
}

Vue.use(Router)

export default new Router({
  routes: [
    {
      path:'/',
      name: 'Container',
      component:loadViews('Container'),
      redirect:'/hello',
      children:[
        {
          path: '/hello',
          name: 'Demo页面1',
          component: loadComponent('HelloWorld')
        },{
          path: '/demo',
          name: 'Demo页面2',
          component: loadViews('Demo')
        },{
          path: '/list',
          name: 'Demo列表页面',
          component: loadViews('List'),
          children:[
            {
              path: '/list/demo',
              name: 'Demo页面2-1',
              component: loadViews('Demo')
            }
          ]
        },
      ]
    }
  ]
})
