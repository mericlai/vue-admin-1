import router from '@/router'
import NProgress from 'nprogress'
import { getCookie,TokenKey } from '@/utils/Cookie'

router.beforeEach((to, from, next) => {
    NProgress.start()
    if (!getCookie(TokenKey)) { 
        window.location.href=process.env.LOGIN_PATH+window.location.href;
    }
    $("html").scrollTop(0);
    next();
});

router.afterEach((to,from)=>{
})