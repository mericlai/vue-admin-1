// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import '@/assets/css/bootstrap.css'
import 'normalize.css/normalize.css'
import '@/assets/css/common.scss'
import '@/permission'
import '../static/layer'
import '../static/theme/default/layer.css'
import Validator from '@/utils/validator-directive'

new Validator(Vue,{timeout:2000}).install(Vue);//加载安装表单验证指令

Vue.config.productionTip = process.env.NODE_ENV === 'production'
if(process.env.NODE_ENV === 'production'){
  Vue.config.errorHandler = function (err, vm, info) {
    // handle error
    // `info` 是 Vue 特定的错误信息，比如错误所在的生命周期钩子
    // 只在 2.2.0+ 可用
    console.log(err);
  }
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
